#!/bin/bash

VER=3.9.0
ARCH=aarch64-linux-gnu
#ARCH=armv7a-linux-gnueabihf
INSTALL_DIR=/usr/lib/llvm-$VER
BIN_DIR=/usr/bin


FILE=clang+llvm-$VER-$ARCH
FN=$FILE.tar.xz
DL_PATH=http://llvm.org/releases/$VER/$FN

echo "This script will install Clang+LLVM $VER for '$ARCH' in '$INSTALL_DIR' and set it to default in '$BIN_DIR'."
read -p "Are you sure? [y/n] " -n 1 -r

echo    # (optional) move to a new line

if [[ $REPLY =~ ^[Yy]$ ]]
then
  echo "Downloading '$DL_PATH'..."
  wget $DL_PATH
  sudo mkdir -p $INSTALL_DIR
  sudo tar --xz -xvf $FN -C $INSTALL_DIR --strip-components=1
  sudo ln -sf $INSTALL_DIR/bin/clang $BIN_DIR/clang
  sudo ln -sf $INSTALL_DIR/bin/clang++ $BIN_DIR/clang++
  sudo ln -sf $INSTALL_DIR/bin/clang-apply-replacements $BIN_DIR/clang-apply-replacements
  sudo ln -sf $INSTALL_DIR/bin/clang-check $BIN_DIR/clang-check
  sudo ln -sf $INSTALL_DIR/bin/clang-query $BIN_DIR/clang-query
  sudo ln -sf $INSTALL_DIR/bin/llvm-tblgen $BIN_DIR/clang-tblgen
  sudo ln -sf $INSTALL_DIR/bin/clang-tidy $BIN_DIR/clang-tidy
  echo "export CC=$BIN_DIR/clang" >> ~/.bash_aliases
  echo "export CXX=$BIN_DIR/clang++" >> ~/.bash_aliases
  rm $FN
  echo "Done!"
fi

